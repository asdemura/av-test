import Vue from "vue";

import App from "./App.vue";
import { router } from "./router.js";
import store from "./store.js";

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    methods: {
        logout() {
            this.$store.dispatch("logout").then(() => {
                if (this.$router.currentRoute.name !== "login") {
                    this.$router.push("login");
                }
            });
        },
    },
    render: (h) => h(App),
}).$mount("#app");

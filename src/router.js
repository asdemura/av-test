import Vue from "vue";
import VueRouter from "vue-router";

import ProductsReview from "./components/ProductsReview.vue";
import About from "./components/About.vue";
import Login from "./components/Login.vue";
import Registration from "./components/Registration.vue";
import Users from "./components/Users.vue";
import store from "./store";

Vue.use(VueRouter);

export const router = new VueRouter({
    mode: "history",
    base: process.env.NODE_ENV === "production" ? "/av-test/" : "/",
    routes: [
        { path: "/", name: "home", redirect: { name: "products" } },
        {
            path: "/products",
            name: "products",
            component: ProductsReview,
            meta: { access: true },
        },
        {
            path: "/about",
            name: "about",
            component: About,
        },
        { path: "/login", name: "login", component: Login },
        {
            path: "/registration",
            name: "registration",
            component: Registration,
        },
        {
            path: "/users",
            name: "users",
            component: Users,
            meta: { access: true, admin: true },
        },
    ],
});

router.beforeEach((to, from, next) => {
    const { access, admin } = to.meta;
    if (access) {
        if (!store.getters.isLoggedIn) {
            return next({ name: "login", query: { returnUrl: to.path } });
        }
        if (admin && !store.getters.isAdmin) {
            return next(from);
        }
    }
    next();
});

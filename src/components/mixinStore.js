export default {
    computed: {
        username() {
            return this.$store.state.username;
        },
        isLoggedIn() {
            return this.$store.getters.isLoggedIn;
        },
        isAdmin() {
            return this.$store.getters.isAdmin;
        },
    },
};

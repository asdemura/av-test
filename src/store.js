import Vue from "vue";
import Vuex from "vuex";
import VueCookies from "vue-cookies";

Vue.use(Vuex);
Vue.use(VueCookies);
if (!sessionStorage.getItem("users")) {
    sessionStorage.setItem(
        "users",
        JSON.stringify({
            admin: { password: "admin" },
        })
    );
}
export default new Vuex.Store({
    state: {
        token: Vue.$cookies.get("token") || "",
        username: Vue.$cookies.get("username") || "",
        users: JSON.parse(sessionStorage.getItem("users")),
    },
    mutations: {
        login(state, payload) {
            const { token, username } = payload;
            state.token = token;
            state.username = username;
            Vue.$cookies.set("token", token);
            Vue.$cookies.set("username", username);
        },
        logout(state) {
            state.token = "";
            state.username = "";
        },
        registration(state, payload) {
            const {
                username,
                password,
                email,
                firstName,
                lastName,
                middleName,
            } = payload;
            state.users[username] = {
                password: password,
                email: email,
                firstName: firstName,
                lastName: lastName,
                middleName: middleName,
            };
            sessionStorage.setItem("users", JSON.stringify(state.users));
        },
    },
    actions: {
        login({ commit }, payload) {
            return new Promise((resolve, reject) => {
                this.dispatch("passwordVerification", payload)
                    .then(() => {
                        const token = "myTestToken";
                        const username = payload.username;
                        commit("login", { token: token, username: username });
                        resolve();
                    })
                    .catch((err) => {
                        reject(err);
                    });
            });
        },
        logout({ commit }) {
            return new Promise((resolve) => {
                Vue.$cookies.remove("token");
                Vue.$cookies.remove("username");
                commit("logout");
                resolve();
            });
        },
        registration({ commit, state }, payload) {
            return new Promise((resolve, reject) => {
                const token = "myTestToken";
                const username = payload.username;
                if (state.users[username]) reject("Этот логин уже занят");
                commit("registration", payload);
                commit("login", { token: token, username: username });
                resolve();
            });
        },
        passwordVerification({ state }, payload) {
            console.log("users", payload);
            return new Promise((resolve, reject) => {
                const username = payload.username;
                const password = payload.password;
                if (!state.users[username]) {
                    reject("Такого логина не существует");
                }
                if (state.users[username].password !== password) {
                    reject("Неправильный пароль");
                }
                resolve("ok");
            });
        },
    },
    getters: {
        isLoggedIn: (state) => !!state.token,
        isAdmin: (state) => state.username === "admin",
    },
});
